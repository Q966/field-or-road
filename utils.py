import os
import numpy as np
from PIL import Image
import cv2
import albumentations as A

import torch
import torch.nn.functional as F

from tqdm import tqdm
from sklearn.metrics import f1_score 

"""
augment_images: Augment train images, not optimized
"""
def augment_images(train_path, train=True):

    print("Starting train images augmentation...")
    
    for directory in os.listdir(train_path):
        path = os.path.join(train_path, directory)
                
        transform = A.Compose([
            A.RandomRain(brightness_coefficient=0.9, drop_width=1, blur_value=1, p=.3),
            A.RandomShadow(num_shadows_lower=1, num_shadows_upper=1, shadow_dimension=5, shadow_roi=(0, 0.5, 1, 1), p=0.2),
            A.Rotate(limit=20, p=.9)
        ])
        
        images_list = list(file for file in os.listdir(path))
        for im in images_list:
            img = np.array(Image.open(os.path.join(path, im)))
            img = transform(image = img)['image']
            img = Image.fromarray(img)

            saving_path = os.path.join(path, f"augmented_{im}")
            if not os.path.exists(saving_path):
                img.save(saving_path)
            else:
                print('Already augmented')
                return
            
    print("Done")


def train_one_epoch(model, device, train_loader, loss_fn, optimizer):
    model.train()
    total_targets, total_preds = [], []
    res = []
    running_loss = 0.0
    batches = 0
    
    t = tqdm(train_loader, desc = 'Train epoch')

    for batch_idx, (imgs, targets) in enumerate(t):
        imgs, targets = imgs.to(device), targets.to(device)
        
        preds = model(imgs) 
        loss = loss_fn(preds, targets)
        
        running_loss += loss.item()
        batches += 1
        
        preds = torch.argmax(F.softmax(preds, dim=1), axis=1)
        res.append((targets.cpu().detach(), preds.cpu().detach()))
        
        # backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
    avg_epoch_loss = round(running_loss / batches, 5)
    
    total_targets = torch.cat([entry[0] for entry in res], 0)
    total_preds = torch.cat([entry[1] for entry in res], 0)
    f1_score_epoch = f1_score(total_targets.numpy(), total_preds.numpy(), average='macro')
    
    return f1_score_epoch, avg_epoch_loss



def test_one_epoch(model, device, test_loader, loss_fn):
    model.eval()  
    total_targets, total_preds = [], []
    running_loss = 0.0
    batches = 0

    t = tqdm(test_loader, desc="Test epoch")
    for batch_idx, (imgs, targets) in enumerate(t):
        imgs, targets = imgs.to(device), targets.to(device)

        preds = model(imgs)
        loss = loss_fn(preds, targets)
        
        running_loss += loss.item()
        batches += 1

        preds = np.argmax(F.softmax(preds, dim=1).cpu().detach().numpy(), axis=1)

        total_targets += targets.cpu().numpy().tolist()
        total_preds += preds.tolist()

    avg_epoch_loss = round(running_loss / batches, 5)
    
    f1_score_epoch = f1_score(total_targets, total_preds, average = 'macro')
    
    print('Test f1_score: {:.2f} '.format(f1_score_epoch*100)) 
    return f1_score_epoch, avg_epoch_loss