import os
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.datasets import ImageFolder
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, Subset

from torchvision.models import resnet18

from sklearn.model_selection import train_test_split
from sklearn.utils.class_weight import compute_class_weight

from utils import augment_images, train_one_epoch, test_one_epoch


# Augment fields and road images
data_path = './train/'
augment_images(data_path)

# 
num_classes = 2
batch_size = 16
img_size = 224


# After quick weather albumentations augmentations, pytorch common ones and model-related ones

normalize = transforms.Normalize(mean = [0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

train_transform = transforms.Compose([
    transforms.Resize(size=(img_size, img_size)),
    transforms.RandomHorizontalFlip(),
    transforms.RandomPerspective(distortion_scale=0.6, p=1),
    transforms.ToTensor(),
    normalize
    ]) 

test_transform = transforms.Compose([
    transforms.Resize(size=(img_size, img_size)),
    transforms.ToTensor(),
    normalize
    ])

train_dataset = ImageFolder(root = data_path, transform = train_transform)
test_dataset = ImageFolder(root = data_path, transform = test_transform)


# Stratifying, weighting, and getting DataLoaders

targets = np.array(train_dataset.targets)
print('Number of images', len(targets))
print('Number of field images', (targets == 0).sum(axis = 0))
print('Number of road images', (targets == 1).sum(axis = 0))

train_idx, test_idx = train_test_split(
    np.arange(len(targets)),
    test_size = 0.2,
    shuffle = True,
    stratify = targets)

def get_weights():
    return compute_class_weight('balanced', classes = np.unique(targets), y = targets)

train_dataset = Subset(train_dataset, train_idx)
test_dataset = Subset(test_dataset, test_idx)
train_loader = DataLoader(train_dataset, batch_size= batch_size, shuffle = True)
test_loader = DataLoader(test_dataset, batch_size= batch_size, shuffle = False)

print(f"Train : { len(train_idx) }, Test : { len(test_idx) }")


# Define model, loss and optimizer

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(f"Running on {device}")

model = resnet18(weights='DEFAULT')
model.fc = nn.Linear(512, num_classes)
model.to(device)

class_weights = torch.FloatTensor(get_weights())
loss_fn = nn.CrossEntropyLoss(weight=class_weights, reduction='mean').to(device)

def trainable_params():
    for param in model.parameters():
        if param.requires_grad:
            yield param

num_trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
print('num_trainable_params:', num_trainable_params)

optimizer = torch.optim.Adam(trainable_params(), lr= 0.001, weight_decay = 1e-5)


# Train

num_epochs = 20
f1_train, f1_test, loss_train, loss_test = [], [], [], []

for epoch in range(num_epochs):
    print(f"Epoch {epoch}/{num_epochs}")
    f1_train_score_epoch, avg_epoch_loss = train_one_epoch(model, device, train_loader, loss_fn, optimizer)
    f1_train.append(f1_train_score_epoch)
    loss_train.append(avg_epoch_loss)
    
    f1_test_score_epoch, test_avg_epoch_loss = test_one_epoch(model, device, test_loader, loss_fn)
    f1_test.append(f1_test_score_epoch)
    loss_test.append(test_avg_epoch_loss)


# Plot Loss

plt.title("Loss")
plt.plot(loss_train, label='Loss train')
plt.plot(loss_test, label='Loss test')
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.legend()
plt.savefig(f'Plot_loss_on_{num_epochs}_epochs.png')


# Plot metrics

plt.figure(figsize=(10,5))
plt.plot(f1_train, label='train')
plt.plot(f1_test, label='test')
plt.ylabel("Score")
plt.xlabel("Epoch")
plt.title("F1 score")
plt.legend()
plt.savefig('Plot_train_test_Metrics.png')


# Plot the 10 inferences with the image, not optimized

fig = plt.figure(figsize = (50, 70))
i = 0

for root, dirnames, filenames in os.walk('./test_images'):
    
    for file in filenames:
        
        plt.subplot(10,1,i+1)
        i += 1
        
        img = Image.open(os.path.join(root, file))
        plt.imshow(img)
        img = test_transform(img).to(device)
        
        pred = model(img.unsqueeze(0))
        
        proba = F.softmax(pred, dim=1).cpu().detach().numpy()
        pred = "Field" if int(np.argmax(proba, axis=1)) == 0 else "Road"

        plt.title(f"Prediction: {pred} ({np.max(proba):.1%})")
        plt.xticks([])
        plt.yticks([])