# Field or Road ?

Code : Image classifier for field or road recognition  

Bonus: summary of research paper

## Getting started : Image classifier for field or road recognition

Before running main, create a conda environment or update an existing one :wink:, then install dependencies and get the data like the following:

``` 
pip install -r requirements.txt
cd your_working_directory
mkdir fieldroad
unzip -q -d ./fieldroad dataset.zip
```
First we have a small and unbalanced dataset (around two roads for 1 field), therefore I did augmentations and allocated the samples evenly based on their classes so that training and validation set have similar ratio of classes.

At this time the leading paradigm in CV is finetuning, with better performances than CNNs from scratch or legacy algorithms. 

Augmented data leads to the best performances when combined with finetuned CNNs. With only two classes and few, quite simple images, I propose a Resnet18 initialized with weights from pretraining on ImageNet, thus adapting the last fully connected layer. 

Weighted cross entropy loss (the intuition here is to give higher weight to minority class and lower weight to majority class in the training, so makes the model aware of the imbalance and the loss outputs the weighted mean of the errors).

Macro average f1 score metric (here I think the two imbalanced classes are equally important and should account equally in the score. On the contrary in weighted averaging, the contribution of each class to the f1 average is weighted by its size, which is useful in binary classification when we are interetested mostly about one class), **getting final minimum test f1 scores between 80 and 99.** I often tend to increase the batch size to 16.

To improve, many possibilities like playing on augmentations, model, grid searching (or Bayesian Search) batch size, learning rate, regularization to prevent overfitting, loss function. 


## Bonus: Summary of the latest research paper I found interesting
https://arxiv.org/abs/2103.13413v1

- Tell us why this publication was interesting to you ?

I discovered about transformers in 2021 and thought that architecture / concept was kind of more generally intelligent / less ad hoc/ handcrafted or « artisanal » we would say in french ; than convolution based architectures. 

This intuition is due to the relative simplicity of the (parallel) processing of sequences, leading the model to understand the global dependencies / similarities of the sequences between each other. It brings finer understanding of the images but often needs lots of data.
Therefore using transformer and convolutional components in a encoder decoder architecture to tasks like semantic segmentation and monocular depth estimation, sounds like an interesting publication to me.


- Summary ?

Dense Prediction Transformer (DPT) are a type of vision transformer for dense prediction tasks. It’s considered dense because we predict the class of each pixel, so the prediction is denser (greater dimensions) than the input image.

The dense predictions here are Semantic Segmentation and Monocular Depth Estimation.  

- Segmentation, or image segmentation, is the task of clustering parts of an image together which belong to the same object class.  
- Monocular Depth Estimation is the task of estimating the depth value (distance relative to the camera) of each pixel given a single (monocular) RGB image.

The convolution in this paper is criticized for losing some granularity and feature resolution during multi scale downsampling, necessitating sequential stacking into very deep architectures to acquire sufficiently broad context and sufficiently high representational power.

To summarize, the transformer here brings a representation at a higher resolution than convolutional, so finer grained predictions. DPT provides a 28% performance increase for general purpose monocular depth estimation and sets a new state of the art for semantic segmentation. Inference speed is competitive as well.

The researchers assemble tokens from various stages of the vision transformer into image-like representations at various resolutions. And progressively combine them into full resolution predictions using a convolutional decoder.

**DPT Architecture :  ViT backbone encoder + Convolutional Decoder**
![](Dense_Prediction_transformer.png "Architecture from paper")

The input image is transformed into tokens (orange) either by extracting non-overlapping patches followed by a linear projection of their flattened representation (DPT-Base and DPT-Large) or by applying a ResNet-50 feature extractor (DPT-Hybrid). The image embedding is augmented with a positional embedding and a patch-independent readout token (red) is added.

The tokens are passed through multiple transformer stages. The tokens are reassembled from different stages into an image-like representation at multiple resolutions (green). Fusion modules (purple) progressively fuse and upsample the representations to generate a fine-grained prediction.

The decoder assembles the set of tokens into image-like feature representations at various resolutions. The feature representations are progressively fused into the final dense prediction. To recover image-like representations from the output tokens of arbitrary layers of the transformer encoder, there’s a three-stage Reassemble operation.


### Experiments, results


- Monocular Depth Estimation, a dense regression problem

They compiled a meta dataset of 1.4 million images, added random horizontal flips for data augmentation. Then first pretrain on a well-curated subset of the data before training on the full dataset. The architeture has the same latency than previous best published one MiDaS.

They did zero-shot transfer to different datasets that were not seen during training : DPT can better reconstruct fine details, while also improving global coherence in areas that are challenging for the convolutional architecture. They did finetuning on small datasets (NYUv2 and KITTI) : DPT matches or improves state-of-the-art performance on both datasets in all metrics, showing it can also be applied on small datasets.

- Semantic Segmentation

Representative of discrete labeling tasks, they trained on ADE20K segmentation dataset. They use multi-scale inference at test time and report both pixel accuracy (pixAcc) as well as mean IoU (mIoU)  
DPT-Hybrid outperformed all existing fully-convolutional architectures. DPT-Large performed slightly worse, likely because of the significantly smaller dataset. Notable : produces cleaner and finer-grained delineations of object boundaries, and strong perf with finetuning on smaller datasets.  
(See results in the paper if interested). 

